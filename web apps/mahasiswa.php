<?php
include 'koneksi.php';
$db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="jquery/jquery-3.4.1.min.js"></script>
    <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <title>Appx 0a</title>
</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">Appx 0a</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="mahasiswa.php">Tabel Mahasiswa<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="prodi.php">Tabel Prodi</a>
                    </li>
                </ul>
            </div>
        </nav>
    <?php
    error_reporting(0);
    $page = $_GET['page'];
    if (empty($page)) :
        ?>
        <br>
        <h3>
            <center>Data Mahasiswa</center>
        </h3>
        <div class="container">
            <table class="table table-bordered table-hover">
                <tr>
                    <th>NIM</th>
                    <th>Nama Mahasiswa</th>
                    <th>ID Prodi</th>
                    <th>Photo</th>
                    <th>Alamat</th>
                </tr>
                <?php
                    foreach ($db->tampil_data_mahasiswa() as $data) {
                        ?>
                    <tr>
                        <td><?php echo $data['nim']; ?></td>
                        <td><?php echo $data['nama']; ?></td>
                        <td><?php echo $data['id_prodi']; ?></td>
                        <td><?php echo $data['photos']; ?></td>
                        <td><?php echo $data['alamat']; ?></td>
                    </tr>
                <?php } ?>
            </table>        
        </div>
        <?php endif; ?>
        <script src="jquery/jquery-3.4.1.min.js"></script>
        <script src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
</body>
</html>
